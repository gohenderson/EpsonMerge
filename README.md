# EpsonMerge

Cadence data isn't included in Epson View's gpx export. It is included in the csv export though. This utility extracts the cadence data from the csv and inserts it into the gpx. The resulting gpx file can then be uploaded to other run tracking svcs.

Usage:

epsonmerge.exe "full path to gpx file (or zipped gpx)" "full path to csv file" "full path to desired output"
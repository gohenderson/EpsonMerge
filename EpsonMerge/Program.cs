﻿/*
Copyright 2018 Greg Henderson

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace EpsonMerge
{
    class Program
    {
        static void Main(string[] args)
        {
            // Should have validation checks but meh--later. Maybe.
            string gpxFile = args[0];
            string csvFile = args[1];
            string outFile = args[2];
            bool isZipFile = false;

            // Memory stream would be better for gpx extraction.
            if (isZipFile = ZipFile.IsZipFile(gpxFile))
            {
                // Extract the gpx zip to a temp location
                var tempPath = Path.GetTempPath() + Guid.NewGuid().ToString().Replace("-", "") + "\\";
                Directory.CreateDirectory(tempPath);

                using (var zip = ZipFile.Read(gpxFile))
                {
                    foreach (var e in zip) // There should only be one file in the archive
                    {
                        gpxFile = tempPath + e.FileName;
                        e.Extract(tempPath, ExtractExistingFileAction.OverwriteSilently);
                    }
                }
            }

            var gpx = XDocument.Load(gpxFile);
            var fileData = File.ReadAllLines(csvFile);

            int startTimeLineIndex = -1;
            for (int i = 0; i < fileData.Length; i++)
                if (fileData[i].StartsWith("TrainingKindId"))
                {
                    startTimeLineIndex = i;
                    break;
                }

            var trainingKindColumns = fileData[startTimeLineIndex].Split(',');

            int startDayColumnIndex = -1, startTimeColumnIndex = -1;
            for (int i = 0; i < trainingKindColumns.Length; i++)
            {
                if (trainingKindColumns[i] == "StartDay")
                    startDayColumnIndex = i;
                else if (trainingKindColumns[i] == "StartTime")
                    startTimeColumnIndex = i;
            }

            var trainingKindColumnData = fileData[startTimeLineIndex + 1].Split(',');

            var startTime = Convert.ToDateTime(trainingKindColumnData[startDayColumnIndex] + " " + trainingKindColumnData[startTimeColumnIndex]);

            int startTimeSecondModifier = 0;
            var cadenceData = fileData
                .FirstOrDefault(x => x.StartsWith("GraphPitch"))
                .Replace("GraphPitch,", "").Trim().Split(',')
                .Select(x => Convert.ToInt32(x))
                .ToDictionary(x => startTime.AddSeconds(startTimeSecondModifier++), x => new Data() { Cadence = (int)(x / (double)2) });

            var trkseg = gpx.Elements().FirstOrDefault(x => x.Name.LocalName == "gpx")
                .Elements().FirstOrDefault(x => x.Name.LocalName == "trk")
                .Elements().FirstOrDefault(x => x.Name.LocalName == "trkseg");

            var trackpoints = new Dictionary<DateTime, List<XElement>>();
            foreach (var trackpoint in trkseg.Elements().Where(x => x.Name.LocalName == "trkpt"))
            {
                DateTime trackpointTime = Convert.ToDateTime(trackpoint.Elements().FirstOrDefault(y => y.Name.LocalName == "time").Value);
                List<XElement> found = null;
                if (trackpoints.TryGetValue(trackpointTime, out found))
                    found.Add(trackpoint);
                else
                    trackpoints.Add(trackpointTime, new List<XElement> { trackpoint });
            }

            foreach (var entry in cadenceData)
            {
                List<XElement> existingTrackpoint = null;
                if (trackpoints.TryGetValue(entry.Key, out existingTrackpoint))
                {
                    var extensions = existingTrackpoint.Elements()
                        .FirstOrDefault(x => x.Name.LocalName == "extensions");

                    if (extensions == null) // Should create an extensions element but I don't feel like doing this right now.
                        continue;

                    XNamespace gpxdata = gpx.Elements().FirstOrDefault(x => x.Name.LocalName == "gpx").Attributes().FirstOrDefault(x => x.Name.LocalName == "gpxdata").Value;
                    var gpxCad = new XElement(gpxdata + "cadence");
                    gpxCad.Value = entry.Value.Cadence.ToString();

                    extensions.Add(gpxCad);
                    existingTrackpoint.Remove();
                    entry.Value.Trackpoints = existingTrackpoint;
                }
            }

            trkseg.RemoveAll();
            foreach (var data in cadenceData.Where(x => x.Value.Trackpoints != null).OrderBy(x => x.Key))
            {
                foreach (var trackpoint in data.Value.Trackpoints)
                    trkseg.Add(trackpoint);
            }

            gpx.Root.Save(outFile);

            if (isZipFile)
            {
                File.Delete(gpxFile);
                Directory.Delete(Path.GetDirectoryName(gpxFile));
            }
        }

        public class Data
        {
            public int Cadence { get; set; }
            public List<XElement> Trackpoints { get; set; }
        }
    }
}
